"use strict";

class Movie {
  constructor(film) {
    this.film = film;
  }

  createMovie() {
    const filmId = document.createElement("div");
    const filmName = document.createElement("h1");
    const description = document.createElement("p");
    const list = document.createElement("ul");

    filmId.innerText = `film id: ${this.film.episodeId}`;
    filmName.innerText = this.film.name;
    description.innerText = this.film.openingCrawl;

    this.film.characters.forEach((element) => {
      const listItem = document.createElement("li");
      listItem.classList.add("spinner");
      list.append(listItem);
      this.renderCharachters(element, listItem);
    });
    return [filmId, filmName, description, list];
  }

  renderCharachters(url, text) {
    fetch(url)
      .then((res) => res.json())
      .then((character) => {
        text.innerText = character.name;
        if (text.innerText === character.name) {
          text.classList.remove("spinner");
        }
      });
  }

  render() {
    const filmElements = this.createMovie();
    const root = document.getElementById("root");
    root.append(...filmElements);
  }
}

function renderMovie() {
  fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((res) => res.json())
    .then((films) => {
      films.forEach((film) => {
        const movie = new Movie(film);
        movie.render();
      });
    });
}

renderMovie();
